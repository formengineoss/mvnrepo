<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <!--<parent>-->
    <!--<groupId>se.sll.formengine</groupId>-->
    <!--<artifactId>form-engine-parent</artifactId>-->
    <!--<version>1.0.0-SNAPSHOT</version>-->
    <!--<relativePath>../pom.xml</relativePath>-->
  <!--</parent>-->

  <groupId>se.sll.formengine</groupId>
  <artifactId>form-engine-api-types</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <packaging>jar</packaging>

  <licenses>
    <license>
      <name>Apache License, Version 2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0</url>
    </license>
  </licenses>

  <properties>
    <!-- keep aligned with "parent" -->
    <lombok.version>1.16.18</lombok.version>
  </properties>

  <dependencies>
    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <version>${lombok.version}</version>
    </dependency>
  </dependencies>

  <distributionManagement>
    <repository>
      <id>form-engine-releases-repo</id>
      <name>form-engine-releases-repo</name>
      <url>git:releases://git@bitbucket.org:formengineoss/mvnrepo.git</url>
    </repository>
    <snapshotRepository>
      <id>form-engine-snapshots-repo</id>
      <name>form-engine-snapshots-repo</name>
      <url>git:snapshots://git@bitbucket.org:formengineoss/mvnrepo.git</url>
    </snapshotRepository>
  </distributionManagement>

  <pluginRepositories>
    <pluginRepository>
      <id>synergian-repo</id>
      <url>https://raw.github.com/synergian/wagon-git/releases</url>
    </pluginRepository>
  </pluginRepositories>

  <build>
    <plugins>
      <plugin>
        <artifactId>maven-source-plugin</artifactId>
        <executions>
          <execution>
            <id>attach-sources</id>
            <phase>deploy</phase>
            <goals>
              <goal>jar-no-fork</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <!-- explicitly define maven-deploy-plugin after maven-source-plugin to force exec order -->
        <artifactId>maven-deploy-plugin</artifactId>
        <executions>
          <execution>
            <id>deploy</id>
            <phase>deploy</phase>
            <goals>
              <goal>deploy</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>

    <extensions>
      <extension>
        <groupId>ar.com.synergian</groupId>
        <artifactId>wagon-git</artifactId>
        <version>0.2.5</version>
      </extension>
    </extensions>
  </build>

</project>
